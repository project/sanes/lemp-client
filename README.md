#### Клонирование репозитория
```
cd ~/www
git clone https://gitflic.ru/project/sanes/lemp-client.git .
```
#### Установка зависимостей
`composer install`
#### Настройка 
```
# Отредактируйте подключение к MySQL и домен сайта
cp .env.example .env
nano .env

# Сгенерируйте ключ
php artisan key:generate
```
#### Миграция и добавление администратора
```
# Отредактируйте Имя, Email и Пароль администратора
nano databases/seeders/DatabaseSeeder.php

# Запустите миграцию 
php artisan mifrate --seed

# Обновление данных администратора
php artisan db:seed
```
#### Очереди
```
touch /etc/systemd/system/queue-main.service
chmod 664 /etc/systemd/system/queue-main.service

[Unit]
Description=queue-service
[Service]
Type=simple
User=lemp
WorkingDirectory=/var/www/lemp/www
ExecStart=/usr/bin/php artisan queue:work
Restart=always
StandardOutput=append:/var/www/lemp/logs/queaue.log
StandardError=append:/var/www/lemp/logs/queue.log
[Install]
WantedBy=multi-user.target

systemctl daemon-reload
```