<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>{{ config('app.name') }}</title>
    <link rel="icon" href="/laravel.png">
    <link rel="stylesheet" href="/css/uikit.min.css" />
    <script src="/js/uikit.min.js"></script>
    <script src="/js/uikit-icons.min.js"></script>
    <script src="/js/turbolinks.min.js"></script>
    <meta name="turbolinks-cache-control" content="no-cache">
    <style>
        .uk-navbar-item, .uk-navbar-nav > li > a, .uk-navbar-toggle {padding: 0 10px;}
        .uk-notification-message {font-size: 16px;}
    </style>        
</head>
<body>
	<div class="uk-container" style="min-height: calc(100vh - 51px);">
		<nav class="uk-navbar" style="border-bottom: 1px solid #e8e8e8;">
			<div class="uk-navbar-left">
				<a href="/" class="uk-logo uk-text-uppercase">{{ config('app.name') }}</a>
			</div>
			<div class="uk-navbar-right">
				<ul class="uk-navbar-nav">
					<li @if(Request::is('site*'))class="uk-active" @endif><a href="{{ route('sites.index') }}"><span class="uk-margin-small-right" uk-icon="list"></span>Сайты</a></li>
					<li>
						<a onclick="document.getElementById('logout').submit();"><span class="uk-margin-small-right" uk-icon="sign-out"></span>Выйти</a>
						<form action="{{ route('logout') }}" method="POST" id="logout">
                        	@csrf
                    	</form> 
					</li>
				</ul>
			</div>
		</nav>
		@yield('content')
	</div>
    <div class="uk-container uk-padding-small uk-text-small uk-text-center uk-text-uppercase">
     {{ config('app.name')}} © @php echo date('Y'); @endphp
    </div>
    @yield('js')	
</body>
</html>