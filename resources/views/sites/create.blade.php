@extends('base')
@section('content')
<div class="uk-grid-small uk-margin" uk-grid>
    <div class="uk-width-expand@m">
        <h4 class="uk-margin-remove">Добавить сайт</h4>
    </div>
</div>
<form action="{{ route('sites.store') }}" method="post" class="uk-form-stacked">
	@csrf

{{-- 	<input type="hidden" name="password" value="">
	<input type="hidden" name="password_db" value=""> --}}
	<div uk-grid>
		<div class="uk-width-1-2@m">
			<div class="uk-margin">
				<div class="uk-form-controls">
					<label for="title" class="uk-form-label">Название</label>
					<input type="text" class="uk-input" id="title" name="title" placeholder="Мой блог" required value="{{ old('title') }}">
					@error('title')<span class="uk-text-small uk-text-danger">{{ $message }}</span>@enderror
				</div>
			</div>
			<div class="uk-margin">
				<div class="uk-form-controls">
					<label for="domain" class="uk-form-label">Домен</label>
					<input type="text" class="uk-input" id="domain" name="domain" placeholder="domain.com" required value="{{ old('domain') }}">
					@error('domain')<span class="uk-text-small uk-text-danger">{{ $message }}</span>@enderror
				</div>
			</div>
			<div class="uk-margin">
				<div class="uk-form-controls">
					<label for="alias" class="uk-form-label">Алиасы</label>
					<input type="text" class="uk-input" id="alias" name="alias" placeholder="www.domain.com blog.domain.com" value="{{ old('alias') }}">
				</div>
			</div>
		</div>
		<div class="uk-width-1-2@m">
			<div class="uk-margin">
				<div class="uk-form-controls">
					<label for="email" class="uk-form-label">Email</label>
					<input type="email" class="uk-input" id="email" name="email" placeholder="mail@domain.com" required value="{{ old('email') }}">
					@error('email')<span class="uk-text-small uk-text-danger">{{ $message }}</span>@enderror
				</div>
			</div>
			<div class="uk-margin">
				<div class="uk-form-controls">
					<label for="directory" class="uk-form-label">Директория</label>
					<input type="text" class="uk-input" id="directory" name="directory" placeholder="public" value="{{ old('directory') }}">
				</div>
			</div>	
			<div class="uk-margin">
				<div class="uk-form-controls">
					<label class="uk-form-label">Приложение</label>
					<div class="uk-child-width-1-2 uk-grid-small" uk-grid>
						<div><label><input class="uk-radio" type="radio" name="app" value="" checked> Не устанавливать</label></div>
						<div><label><input class="uk-radio" type="radio" name="app" value="wordpress"> Wordpress</label></div>
						<div><label><input class="uk-radio" type="radio" name="app" value="laravel"> Laravel</label></div>
						<div><label><input class="uk-radio" type="radio" name="app" value="lumen"> Lumen</label></div>
					</div>
				</div>
			</div>		
		</div>
	</div>
	<div class="uk-margin">
		<button class="uk-button uk-button-default uk-margin-small-right">Добавить</button>
		<a href="{{ route('sites.index') }}" class="uk-button uk-button-primary">Отмена</a>
	</div>
</form>
<script>
    function genpass(){
    const rand = Math.random().toString(36).slice(2);
    document.getElementsByName('password')[0].value = rand;        
    }
    function genpassdb(){
    const rand = Math.random().toString(36).slice(2);
    document.getElementsByName('password_db')[0].value = rand;        
    }
    genpass()
    genpassdb()        	
</script>
@endsection