@extends('base')
@section('content')
<div class="uk-grid-small uk-margin" uk-grid>
    <div class="uk-width-expand@m">
        <h4 class="uk-margin-remove">{{ $site->title }}</h4>
    </div>
    <div class="uk-width-auto">
           
    </div>
</div>
<div class="uk-grid-medium" uk-grid>
	<div class="uk-width-1-2@m">
		<div class="uk-grid-small" uk-grid>
		    <div class="uk-width-expand" uk-leader>Домен</div>
		    <div class="uk-link-muted">
		    	@isset($site->alias)
		    	<a href="#" class="uk-icon-link uk-margin-small-right" uk-icon="eye"></a>
				<div uk-dropdown="mode: click">
					<ul class="uk-nav uk-nav-default">
						<li class="uk-nav-header">Псевдонимы</li>
						<li class="uk-nav-divider"></li>
						<li>{{ $site->alias }}</li>
					</ul>
				</div>	
				@endisset		    	
		    	<a href="http://{{ $site->domain }}" target="_blank">{{ $site->domain }}</a>
		    </div>
		</div>				
		<div class="uk-grid-small" uk-grid>
		    <div class="uk-width-expand" uk-leader>Имя пользователя</div>
		    <div>
		    	<a href="#" class="uk-icon-link uk-margin-small-right" uk-icon="eye"></a>
				<div uk-dropdown="mode: click; pos:left-top;">
					<ul class="uk-nav uk-nav-default uk-text-nowrap">
						<li class="uk-nav-header">Реквизиты доступа</li>
						<li class="uk-nav-divider"></li>
						<li><span class="uk-text-muted">Пароль пользователя:</span> {{ \Crypt::decryptString($site->password) }}</li>
						<li><span class="uk-text-muted">Пароль пользователя MySQL:</span> {{ \Crypt::decryptString($site->password_db) }}</li>
					</ul>
				</div>		    	
		    	{{ $site->user }}
		    </div>
		</div>						
		<div class="uk-grid-small" uk-grid>
		    <div class="uk-width-expand" uk-leader>Версия PHP</div>
		    <div>{{ $site->php }}</div>
		</div>		
		<div class="uk-grid-small" uk-grid>
		    <div class="uk-width-expand" uk-leader>Директория</div>
		    <div>/var/www/{{ $site->user }}/www/{{ $site->directory }}</div>
		</div>		
		<div class="uk-grid-small" uk-grid>
		    <div class="uk-width-expand" uk-leader>Email</div>
		    <div>{{ $site->email }}</div>
		</div>	
	</div>
	<div class="uk-width-1-2@m">				
		<div class="uk-grid-small" uk-grid>
		    <div class="uk-width-expand" uk-leader>Сертификат SSL</div>
		    <div><span uk-icon="eye" uk-tooltip="title: {{ $site->cert }}; pos: left;"></span></div>
		</div>				
		<div class="uk-grid-small" uk-grid>
		    <div class="uk-width-expand" uk-leader>Ключ SSL</div>
		    <div><span uk-icon="eye" uk-tooltip="title: {{ $site->key }}; pos: left;"></span></div>
		</div>							
		<div class="uk-grid-small" uk-grid>
		    <div class="uk-width-expand" uk-leader>Шаблон PHP-FPM</div>
		    <div>{{ $site->pool }}</div>
		</div>	
		<div class="uk-grid-small" uk-grid>
		    <div class="uk-width-expand" uk-leader>Шаблон Nginx</div>
		    <div>{{ $site->nginx }}</div>
		</div>		
		<div class="uk-grid-small" uk-grid>
		    <div class="uk-width-expand" uk-leader>Диск</div>
		    <div>{{ round($du/1024/1024, 2) }} Gb</div>
		</div>										
	</div>
	<div class="uk-width-1-2@m">
		<a href="{{ route('sites.index') }}" class="uk-button uk-button-default">Назад</a>
		@if($site->status === 'active')
		<a href="{{ route('sites.edit', $site->id) }}" class="uk-button uk-button-default">Редактировать</a>
		<a onclick="destroySite('{{ $site->id }}', '{{ $site->title }}');" class="uk-button uk-button-danger">Удалить</a>
	        <form action="{{ route('sites.destroy', $site->id) }}" method="post" id="destroy">
	            @method('DELETE')
	            @csrf
	            <input type="hidden" name="profile" value="1">
	        </form>
	    @endif    
	</div>  	
</div>
@endsection
@section('js')
<script> 
    function destroySite(id, name){
        let confirmDestroy = confirm('Удалить сайт ' + name + '?');
        let destroyForm = document.getElementById('destroy');
        if (confirmDestroy) {
            destroyForm.submit();
        }
    }   
</script>
@endsection