@if($sites->count() > 0)
<table class="uk-table uk-table-small uk-table-middle uk-table-striped uk-table-hover uk-text-small">
    <thead>
        <tr>
            <th class="uk-table-shrink">ID</th>
            <th>Название</th>
            <th class="uk-table-shrink">Домен</th>
            <th class="uk-table-shrink">User</th>
            <th class="uk-table-shrink">Статус</th>
        </tr>
    </thead>
    <tbody>
        @foreach($sites as $site)
        <tr>
            <td>{{ $site->id }}</td>
            <td class="uk-table-link"><a href="{{ route('sites.show', $site->id) }}" class="uk-link-reset">{{ $site->title }}</a></td>
            <td class="uk-table-shrink uk-text-nowrap"><a href="http://{{ $site->domain }}" class="uk-link-reset" target="_blank">{{ $site->domain }}</a></td>
            <td>{{ $site->user }}</td>
            <td class="uk-text-right">
                @if($site->status === 'locked')
                <a href="{{ route('sites.index') }}" class="uk-icon-link locked" uk-icon="clock"></a>
                @elseif($site->status === 'error')
                <span uk-icon="warning"></span>
                @else
                <span uk-icon="check"></span>
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@else
<div class="uk-text-center">
    Ничего не найдено.
</div>
@endif
