@extends('base')
@section('content')
<div class="uk-grid-small uk-margin" uk-grid>
    <div class="uk-width-expand@m">
        <h4 class="uk-margin-remove">Сайты</h4>
    </div>
    <div class="uk-width-auto">
		<ul class="uk-iconnav">
            <li class="uk-text-uppercase"><a href="{{ route('sites.create') }}"><span class="uk-margin-small-right" uk-icon="icon: plus"></span>Добавить</a></li>
		</ul>              
    </div>
</div>
<div id="data">
    @include('sites.index-data') 
</div>
@endsection
@section('js')
<script>

setInterval( function() {
    fetch('sites/index-data').then(function (response) {
        return response.text();
    }).then(function (html) {
        document.querySelector('#data').innerHTML = html;
    }).catch(function (err) {
        console.warn('Error', err);
    }); 

}, 10000);
    @if (Session::has('job-created'))
    UIkit.notification({
        message: 'Задание добавлено в очередь',
        timeout: 10000
    });
    @endif   
</script>
@endsection