@extends('base')
@section('content')
<div class="uk-grid-small uk-margin" uk-grid>
    <div class="uk-width-expand@m">
        <h4 class="uk-margin-remove">Изменить сайт</h4>
    </div>
</div>
<form action="{{ route('sites.update', $site->id) }}" method="post" class="uk-form-stacked">
	@csrf
	@method('put')	<div uk-grid>
		<div class="uk-width-1-2@m">
			<div class="uk-margin">
				<div class="uk-form-controls">
					<label for="title" class="uk-form-label">Название</label>
					<input type="text" class="uk-input" id="title" name="title" placeholder="Мой блог" value="{{ $site->title }}" required>
					@error('title')<span class="uk-text-small uk-text-danger">{{ $message }}</span>@enderror
				</div>
			</div>
			<div class="uk-margin">
				<div class="uk-form-controls">
					<label for="domain" class="uk-form-label">Домен</label>
					<input type="text" class="uk-input" id="domain" name="domain" placeholder="domain.com" value="{{ $site->domain }}" required>
					@error('domain')<span class="uk-text-small uk-text-danger">{{ $message }}</span>@enderror
				</div>
			</div>
			<div class="uk-margin">
				<div class="uk-form-controls">
					<label for="alias" class="uk-form-label">Алиасы</label>
					<input type="text" class="uk-input" id="alias" name="alias" placeholder="www.domain.com blog.domain.com" value="{{ $site->alias }}">
				</div>
			</div>
			<div class="uk-margin">
				<div class="uk-form-controls">
					<label for="email" class="uk-form-label">Email</label>
					<input type="email" class="uk-input" id="email" name="email" placeholder="mail@domain.com" value="{{ $site->email }}" required>
					@error('email')<span class="uk-text-small uk-text-danger">{{ $message }}</span>@enderror
				</div>
			</div>
			<div class="uk-margin">
				<div class="uk-form-controls">
					<label for="directory" class="uk-form-label">Директория</label>
					<input type="text" class="uk-input" id="directory" name="directory" placeholder="public" value="{{ $site->directory }}">
				</div>
			</div>	
		</div>
		<div class="uk-width-1-2@m">
			<div class="uk-margin">
				<div class="uk-form-controls">
					<label for="cert" class="uk-form-label">Сертификат SSL</label>
					<input type="text" class="uk-input" id="cert" name="cert" placeholder="/etc/letsencrypt/localhost.cert" value="{{ $site->cert }}">
				</div>
			</div>	
			<div class="uk-margin">
				<div class="uk-form-controls">
					<label for="key" class="uk-form-label">Закрытый ключ SSL</label>
					<input type="text" class="uk-input" id="key" name="key" placeholder="/etc/letsencrypt/localhost.key" value="{{ $site->key }}">
				</div>
			</div>	
			<div class="uk-margin">
				<div class="uk-form-controls">
					<label for="nginx" class="uk-form-label">Шаблон Nginx</label>
					<input type="text" class="uk-input" id="nginx" name="nginx" placeholder="vhost" value="{{ $site->nginx }}">
				</div>
			</div>	
			<div class="uk-margin">
				<div class="uk-form-controls">
					<label for="pool" class="uk-form-label">Шаблон PHP-FPM</label>
					<input type="text" class="uk-input" id="pool" name="pool" placeholder="user" value="{{ $site->pool }}">
				</div>
			</div>	
			<div class="uk-margin">
				<div class="uk-form-controls">
					<label for="php" class="uk-form-label">Версия PHP</label>
					<div class="uk-child-width-1-3@m uk-grid-small" uk-grid>
						<div><label><input class="uk-radio" type="radio" name="php" value="7.4" @if($site->php === "7.4")checked @endif> PHP 7.4</label></div>
						<div><label><input class="uk-radio" type="radio" name="php" value="8.0" @if($site->php === "8.0")checked @endif> PHP 8.0</label></div>
						<div><label><input class="uk-radio" type="radio" name="php" value="8.1" @if($site->php === "8.1")checked @endif> PHP 8.1</label></div>
					</div>
				</div>
			</div>		
		</div>
	</div>
	<div class="uk-margin">
		<button class="uk-button uk-button-default uk-margin-small-right">Обновить</button>
		<a href="{{ route('sites.show', $site->id) }}" class="uk-button uk-button-primary">Отмена</a>
	</div>
</form>
@endsection