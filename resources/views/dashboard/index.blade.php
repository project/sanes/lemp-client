@extends('base')
@section('content')
<div class="uk-child-width-1-2@m uk-margin-top" uk-grid>
	<div>
		<h4>Параметры сервера</h4>
	    <div class="uk-grid-small" uk-grid>
	        <div class="uk-width-expand" uk-leader>Hostname</div>
	        <div>{{ $monit->hostname }}</div>
	    </div>
	    <div class="uk-grid-small" uk-grid>
	        <div class="uk-width-expand" uk-leader>CPU Core</div>
	        <div>{{ $monit->totalCpu }}</div>
	    </div>
	    <div class="uk-grid-small" uk-grid>
	        <div class="uk-width-expand" uk-leader>RAM</div>
	        <div>{{ $monit->totalRam }} Mb</div>
	    </div>
	    <div class="uk-grid-small" uk-grid>
	        <div class="uk-width-expand" uk-leader>Disk</div>
	        <div>{{ $monit->totalDisk }} Gb</div>
	    </div>
	    <div class="uk-grid-small" uk-grid>
	        <div class="uk-width-expand" uk-leader>Uptime</div>
	        <div>{{ $monit->d }} д. {{ $monit->h }} ч. {{ $monit->m }} м.</div>
	    </div>
	</div>
	<div id="la">
		@include('dashboard.la')
	</div>
</div>
@endsection
@section('js')
<script>
setInterval( function() {
	fetch('/la').then(function (response) {
		return response.text();
	}).then(function (html) {
		document.querySelector('#la').innerHTML = html;
	}).catch(function (err) {
		document.location.reload();
	});	

}, 10000);
</script>
@endsection