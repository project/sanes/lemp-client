<div class="uk-background-muted uk-padding-small">
	<h4>Текущая нагрузка</h4>
	<div class="uk-margin">
		<div class="uk-flex uk-flex-between">
			<span class="">CPU</span>
			<span class="">{{ round($monit->cpu, 0) }}%</span>
		</div>
		<progress class="uk-progress uk-margin-remove-top" style="background: #efefef;" value="{{ $monit->cpu }}" max="100"></progress>
	</div>

	<div class="uk-margin">
		<div class="uk-flex uk-flex-between">
			<span class="">RAM</span>
			<span class="">{{ $monit->ram }}%</span>
		</div>
		<progress class="uk-progress uk-margin-remove-top" style="background: #efefef;" value="{{ $monit->ram }}" max="100"></progress>
	</div>

	<div class="uk-margin">
		<div class="uk-flex uk-flex-between">
			<span class="">Disk</span>
			<span class="">{{ $monit->disk }}%</span>
		</div>
		<progress class="uk-progress uk-margin-remove-top" style="background: #efefef;" value="{{ $monit->disk }}" max="100"></progress>
	</div>
</div>