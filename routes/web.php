<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\DashboardController::class, 'index'])->middleware('auth')->name('dashboard');
Route::get('/la', [App\Http\Controllers\DashboardController::class, 'la'])->middleware('auth')->name('dashboard.la');
Route::get('sites/index-data', [App\Http\Controllers\SiteController::class, 'indexData'])->middleware('auth')->name('sites.index-data');
Route::resource('sites', App\Http\Controllers\SiteController::class)->middleware('auth');
