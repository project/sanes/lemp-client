<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{   
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sites', function (Blueprint $table) {
            $table->id();
            $table->string('title', 50);
            $table->string('user', 50)->nullable();
            $table->string('domain', 50);
            $table->text('alias')->nullable();
            $table->enum('status', ['locked', 'active', 'error'])->default('locked');
            $table->string('email', 50);
            $table->string('cert', 255);
            $table->string('key', 255);
            $table->string('directory', 50)->nullable();
            $table->string('app', 50)->nullable();
            $table->string('nginx', 50);
            $table->string('php', 10);
            $table->string('pool', 50);
            $table->text('password');
            $table->text('password_db');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sites');
    }
};
