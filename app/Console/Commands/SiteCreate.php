<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Site;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Illuminate\Support\Facades\Crypt;

class SiteCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'site:create {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $site = Site::find($this->argument('id'));

        $process = new Process([
            'sudo', '/usr/bin/ansible-playbook', '/var/www/cpadmin/cpadmin-ansible/site-create.yml', '--extra-vars', 
            'username=vh-'.$site->id,
            'userpass='.Crypt::decryptString($site->password),
            'db_pass='.Crypt::decryptString($site->password_db),
            'domain='.$site->domain,
            'directory='.$site->directory
        ]);
        $process->run();
        if (!$process->isSuccessful()) {
            $site->status = 'error';
        }
        else {
            $site->status = 'active';
            $site->password = $process->getOutput();
        }
        $site->update();        
        return 0;
    }
}
