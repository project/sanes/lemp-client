<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    use HasFactory;

    protected $fillable = [

    ];     

    protected $attributes = [
        'cert' => '/etc/letsencrypt/localhost.cert',
        'key' => '/etc/letsencrypt/localhost.key',
        'nginx' => 'vhost',
        'php' => '8.1',
        'pool' => 'user',
    ];    
}
