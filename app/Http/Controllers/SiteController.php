<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Site;
use App\Jobs\SiteCreate;
use App\Jobs\SiteUpdate;
use App\Jobs\SiteDestroy;
use Illuminate\Support\Facades\Crypt;
use App\Http\Requests\SiteCreateRequest;
use App\Http\Requests\SiteUpdateRequest;
use Str;

class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sites = Site::orderByDesc('created_at')->get();
        return view('sites.index', ['sites' => $sites]);
    }

    public function indexData()
    {
        $sites = Site::orderByDesc('created_at')->get();
        return view('sites.index-data', ['sites' => $sites]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sites.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SiteCreateRequest $request)
    {
        $site = new Site;
        $site->title = $request->title;
        $site->domain = $request->domain;
        $site->alias = $request->alias;
        $site->email = $request->email;
        $site->directory = $request->directory;
        $site->app = $request->app;
        $site->password = Crypt::encryptString(Str::random(8));
        $site->password_db = Crypt::encryptString(Str::random(8));
        $site->save();
        
        // return json_encode($site);
        SiteCreate::dispatch($site);
        return redirect(route('sites.index'))->with('job-created', '');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Site $site)
    {
        $du = exec("sudo du -sk /var/www/vh-".$site->id." |cut -f 1");
        // dd($du);
        return view('sites.show', ['site' => $site, 'du' => $du]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($site)
    {
        $site = Site::where('id', $site)->where('status', 'active')->firstOrFail();
        return view('sites.edit', ['site' => $site]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SiteUpdateRequest $request, $site)
    {
        $site = Site::where('id', $site)->where('status', 'active')->firstOrFail();
        $site->title = $request->title;
        $site->status = 'locked';
        $site->domain = $request->domain;
        $site->alias = $request->alias;
        $site->email = $request->email;
        $site->nginx = $request->nginx;
        $site->php = $request->php;
        $site->pool = $request->pool;
        $site->directory = $request->directory;
        $site->key = $request->key;
        $site->cert = $request->cert;
        if ($request->password) {
            $site->password = Crypt::encryptString($request->password);
        }
        if ($request->password_db) {
            $site->password_db = Crypt::encryptString($request->password_db);
        }
        $site->update();
        SiteUpdate::dispatch($site);
        return redirect(route('sites.index'))->with('job-created', '');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($site)
    {
        $site = Site::where('id', $site)->where('status', 'active')->firstOrFail();
        $site->status = 'locked';
        $site->update();
        SiteDestroy::dispatch($site->id);
        return redirect(route('sites.index'))->with('job-created', '');
    }
}
