<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $monit = exec('python3 /var/www/lemp/ansible/monit.py');
        return view('dashboard.index', ['monit' => json_decode($monit)]);
    }
    public function la()
    {
        $monit = exec('python3 /var/www/lemp/ansible/monit.py');
        return view('dashboard.la', ['monit' => json_decode($monit)]);
    }
}
