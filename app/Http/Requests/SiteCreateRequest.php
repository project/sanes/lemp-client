<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\FQDN;

class SiteCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title' => 'required|min:5|max:50',
            'domain' => [
                            'required',
                            new FQDN(),
                            'unique:sites',
                        ],
            'email' => 'required|email',
        ];
    }


    // protected function prepareForValidation()
    // {
    //     $this->merge([
    //         'domain' => parse_url($this->domain, PHP_URL_HOST),
    //     ]);
    // }    
}
