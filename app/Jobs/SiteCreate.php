<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Site;
use Illuminate\Support\Facades\Crypt;

class SiteCreate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $site;

    public function __construct(Site $site)
    {
        $this->site = $site;
    }


    public function handle()
    {
        $site = Site::find($this->site->id);

        exec('sudo ansible-playbook /var/www/lemp/ansible/site-create.yml --extra-vars "username=vh-'.$site->id.' userpass='.Crypt::decryptString($site->password).' db_pass='.Crypt::decryptString($site->password_db).' domain='.$site->domain.' directory='.$site->directory.' app='.$site->app.'"', $output, $code);
        
        if($code === 0)
        {
            $site->user = 'vh-'.$site->id;
            $site->status = 'active';
        }
        else
        {
            $site->user = 'vh-'.$site->id;
            $site->status = 'error';
            $this->fail();
        }
        $site->update();
    }
}
